from  geopy.geocoders import ArcGIS
from mysql import connector
from math import cos, asin, sqrt

conn = connector.connect(
  host="localhost",
  user="root",
  passwd="100560",
  database='coyotes'
)

geolocator = ArcGIS()




cursor = conn.cursor()
cursor.execute("select * from atleta where lat is null or lon is null")
atl_pend = cursor.fetchall()


coordinates = []

for atleta in atl_pend:

    address = atleta[5] + ", " + atleta[6] + ", " + atleta[7] + "-MT, Brasil"
    print(address)
    loc = geolocator.geocode(address)
    if loc:
        print(str(loc.latitude) + ", " + str(loc.longitude))
        cursor.execute("update atleta set lat = " + str(loc.latitude) + ", lon = " + str(loc.longitude) + " where id = " + str(atleta[0]))
        conn.commit()
        # coordinates.append([atleta[0], loc.latitude, loc.longitude])



cursor.execute("select id, lat, lon from atleta")
atletas = cursor.fetchall()

cursor.execute("select id, lat, lon from academia")
academias = cursor.fetchall()


def distance(lat1, lon1, lat2, lon2):
    p = 0.017453292519943295     #Pi/180
    a = 0.5 - cos((lat2 - lat1) * p)/2 + cos(lat1 * p) * cos(lat2 * p) * (1 - cos((lon2 - lon1) * p)) / 2
    return 12742 * asin(sqrt(a)) #2*R*asin...



for atleta in atletas:
    for academia in academias:
        sql = "INSERT INTO coyotes.atleta_academia	(id_atleta, id_academia, distancia) VALUES (" + str(atleta[0]) + ", " + str(academia[0]) + ", " + str(distance(float(atleta[1]), float(atleta[2]), float(academia[1]), float(academia[2]))) + ")"
        print(sql)
        cursor.execute(sql)
        conn.commit()



cursor.close()
conn.close()


